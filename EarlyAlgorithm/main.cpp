#include <iostream>
#include <string>
#include <vector>
#include <set>
#include "Test.h"

struct Rule {
    std::string left;
    std::string right;
    
    bool operator==(const Rule& rule) const {
        return (rule.left == left) && (rule.right == right);
    }
    Rule (std::string& _left, std::string& _right) : left(_left), right(_right) {}
};

struct Event {
    std::string left;
    std::string right;
    size_t position;
    size_t pointPosition;
    
    bool operator==(const Event& event) const {
        bool equalStrs = (event.left == left) && (event.right == right);
        bool equalPoses = (event.position == position) && (event.pointPosition == pointPosition);
        return equalStrs && equalPoses ;
    }
    
    bool operator<(const Event& other) const {
        return left < other.left;
    }
    
    Event (std::string& _left, std::string& _right, size_t pos, size_t pointPos) : left(_left), right(_right), position(pos), pointPosition(pointPos) {}
    
    Event (char _left, std::string& _right, size_t pos, size_t pointPos) : right(_right), position(pos), pointPosition(pointPos)
    {
        left += _left;
    }
    
};


class Algo {
public:
    Algo (std::string& _word);
    
    void addRule (Rule& rule);
    void addEvent (Event event, size_t num);
    
    // собственно функции алгоритма
    void scan (size_t eventNum, char symbol);
    void predict (size_t eventNum);
    void complete (size_t eventNum);
    
    // распознавание слова
    bool recognizeTheWord (std::string& word);
    
private:
    std::vector<Rule> rules;
    std::vector< std::vector<Event> > events;
    std::string word;
    
    bool exist (const Event& event, size_t pos);
};

bool Algo::exist(const Event &event, size_t pos) {
    for (auto tmp : events[pos]) {
        if (tmp == event) {
            return true;
        }
    }
    return false;
}

Algo::Algo (std::string& _word) : word(_word), events(_word.length() + 1) {
    std::string left = "S#", right = "S";
    Event start (left, right, 0, 0);
    events[0].push_back(start);
}

void Algo::addRule(Rule &rule) {
    rules.push_back(rule);
}

bool compare (std::string& str, char sym) {
    if (str.length() == 1) {
        return (str[0] == sym);
    }
    
    return false;
}

void Algo::addEvent(Event event, size_t num) {
    if (!exist(event, num))
        events[num].push_back(event);
}

void Algo::predict(size_t eventNum) {
    std::vector<Event> newEvents;
    
    for (auto event : events[eventNum]) {
        if (event.pointPosition < event.right.length()) {
            char sym = event.right[event.pointPosition];
            
            for (auto rule : rules) {
                if (compare(rule.left, sym)) {
                    Event tmpEvent (sym, rule.right, eventNum, 0);
                    newEvents.push_back(tmpEvent);
                }
            }
            
        }
    }
    
    for (auto event : newEvents) {
        addEvent(event, eventNum);
    }
    
}

void Algo::scan(size_t eventNum, char symbol) {
    
    for (auto event : events[eventNum]) {
        if (event.right[event.pointPosition] == symbol) {
            Event tmp (event.left, event.right, event.position, event.pointPosition+1); // ???
            addEvent(tmp, eventNum + 1);
        }
    }
}

void Algo::complete(size_t eventNum) {
    std::vector<Event> newEvents;
    
    for (auto event : events[eventNum]) {
        size_t tmpEventNum = event.position;
        
        if (event.pointPosition == event.right.length()) {
            
            for (auto tmpEvent : events[tmpEventNum]) {
                Event cur(tmpEvent.left, tmpEvent.right, tmpEvent.position, tmpEvent.pointPosition + 1);
                newEvents.push_back(cur);
            }
            
        }
        
    }
    
    for (auto event : newEvents) {
        addEvent(event, eventNum);
    }
    
}

bool Algo::recognizeTheWord(std::string& word) {
    size_t startLen = events[0].size();
    predict(0);
    complete(0);
    
    size_t curLen = events[0].size();
    
    while (curLen != startLen) {
        startLen = curLen;
        predict(0);
        complete(0);
        curLen = events[0].size();
    }
    
    for (size_t i = 1; i < word.length() + 1; i++) {
        scan(i-1, word[i-1]);
        startLen = events[i].size();
        predict(i);
        complete(i);
        curLen = events[i].size();
        
        while (curLen != startLen) {
            startLen = curLen;
            predict(i);
            complete(i);
            curLen = events[i].size();
        }
        
        
    }
    
    std::string left = "S#";
    std::string right = "S";
    Event succeedEvent (left, right, 0, 1);
    
    for (auto event : events[word.length()]) {
        if (event == succeedEvent) {
            return true;
        }
    }
    
    return false;
}

void firstTestGrammatic(Algo& earley) {
    std::string left = "S";
    std::string right = "aS";
    
    Rule rule(left, right);
    earley.addRule(rule);
    
    right = "b";
    Rule rule1 (left, right);
    earley.addRule(rule1);
    
    left = "S#";
    right = "S";
    Rule terminateRule (left, right);
    earley.addRule(terminateRule);
}

void secondTestGrammatic(Algo& earley) {
    std::string left = "S";
    std::string right = "aSaS";
    
    Rule rule(left, right);
    earley.addRule(rule);
    
    right = "b";
    Rule rule1 (left, right);
    earley.addRule(rule1);
    
    right = "a";
    Rule rule2 (left, right);
    earley.addRule(rule2);
    
    left = "S#";
    right = "S";
    Rule terminateRule (left, right);
    earley.addRule(terminateRule);
}

bool testing () {
    bool succeed = true;
    std::string testWord = "aaab";
    Algo algo(testWord);
    firstTestGrammatic(algo);
    
    if (algo.recognizeTheWord(testWord)) {
        std::cout << "FIRST TEST PASSED\n";
    } else {
        std::cout << "FIRST TEST FAILED\n";
        succeed = false;
    }
    
    testWord = "abaa";
    algo = Algo(testWord);
    firstTestGrammatic(algo);
    if (!algo.recognizeTheWord(testWord)) {
        std::cout << "SECOND TEST PASSED\n";
    } else {
        std::cout << "SECOND TEST FAILED\n";
        succeed = false;
    }
    
    testWord = "acabcba";
    algo = Algo(testWord);
    firstTestGrammatic(algo);
    if (!algo.recognizeTheWord(testWord)) {
        std::cout << "THIRD TEST PASSED\n";
    } else {
        std::cout << "THIRD TEST FAILED\n";
        succeed = false;
    }
    
    testWord = "aaaabab";
    algo = Algo(testWord);
    secondTestGrammatic(algo);
    if (algo.recognizeTheWord(testWord)) {
        std::cout << "FOURTH TEST PASSED\n";
    } else {
        std::cout << "FOURTH TEST FAILED\n";
        succeed = false;
    }
    
    testWord = "abaa";
    algo = Algo(testWord);
    secondTestGrammatic(algo);
    if (algo.recognizeTheWord(testWord)) {
        std::cout << "FIFTH TEST PASSED\n";
    } else {
        std::cout << "FIFTH TEST FAILED\n";
        succeed = false;
    }
    
    testWord = "aababab";
    algo = Algo(testWord);
    secondTestGrammatic(algo);
    if (algo.recognizeTheWord(testWord)) {
        std::cout << "SIXTH TEST PASSED\n";
    } else {
        std::cout << "SIXTH TEST FAILED\n";
        succeed = false;
    }
    
    testWord = "aababa";
    algo = Algo(testWord);
    secondTestGrammatic(algo);
    if (!algo.recognizeTheWord(testWord)) {
        std::cout << "SEVENTH TEST PASSED\n";
    } else {
        std::cout << "SEVENTH TEST FAILED\n";
        succeed = false;
    }
    
    return succeed;
}

int main() {
    if (testing()) {
        std::cout << "TESTING SUCCEED\n";
    } else {
        std::cout << "TESTING FAILED\n";
        return 0;
    }
    
    std::cout << "Введите слово - ";
    std::string word;
    std::cin >> word;
    
    Algo earley (word);
    
    std::cout << "\nКоличество правил - ";
    size_t num = 0;
    std::cin >> num;
    
    std::cout << "\nПравила в виде A -> B\n";
    for (size_t i = 0; i < num; i++) {
        std::string left, trush, right;
        std::cin >> left >> trush >> right;
        Rule rule(left, right);
        
        earley.addRule(rule);
    }
    
    std::string left = "S#", right = "S";
    Rule terminateRule (left, right);
    earley.addRule(terminateRule);
    
    
    if (earley.recognizeTheWord(word)) {
        std::cout << "Слово распознано!";
    } else {
        std::cout << "Слово не распознано!";
    }
    
    return 0;
}
